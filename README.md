# senv module 0.0.1

senv is a Python console app project based on some portions of the
[__ae__ namespace(Application Environment)](https://ae.readthedocs.io "ae on rtd").

the source code is available at [Gitlab](https://gitlab.com/AndiEcker/senv).

