features and use-cases
**********************

this project provides a simple way to backup the environment files that are not included in any
git repository.

.. note::
    this is a very personal version with all the saved files hard-coded.


examples
********

the password can be specified with the password option (long and short option)::

    senv --password my_secret_password
    -or-
    senv -p my_secret_password

running this script without any arguments will prompt the user to enter a password.
