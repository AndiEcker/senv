""" save linux user environment into a zip file for backup
"""
import getpass
import os

from ae.base import DOTENV_FILE_NAME, now_str                                               # type: ignore
from ae.paths import Collector                                                              # type: ignore
from ae.console import sh_exec, ConsoleApp                                                  # type: ignore


__version__ = '0.0.4'


def main():
    """ main function """
    cae = ConsoleApp()
    cae.add_option('password', "password to encrypt the archive", "")
    cae.run_app()                                           # parse command line arguments

    password = cae.get_option('password') or getpass.getpass("Enter password:")
    if not password:
        cae.po("  **  no password specified")
        cae.shutdown(30)

    coll = Collector()

    coll.collect('~/src/**/' + DOTENV_FILE_NAME)
    coll.collect('~/src/**/.*.json')    # Google Drive credential json files

    coll.collect('~/.bash_history')
    coll.collect('~/.bashrc')
    coll.collect('~/.gitconfig')
    coll.collect('~/.lmmsrc.xml')
    coll.collect('~/.pgpass')
    coll.collect('~/.profile')
    coll.collect('~/.python_history')
    coll.collect('~/.wget-hsts')
    coll.collect('~/.xinputrc')

    # already included from ~/src/tools: coll.collect('~/bin', select=('*.py', '*.sh'), only_first_of=()
    coll.collect('~/.config/**', select=('*.cfg', '.*.cfg', '*.conf', '*.ini'), only_first_of=())
    coll.collect('~/src/doc/**', select=('*', '.*'), only_first_of=())
    coll.collect('~/src/tools/**', select=('*.py', '*.sh'), only_first_of=())

    collected_files = list(set(coll.files))
    zip_file_name = f'.senv_{now_str()}.7z'
    args = ['a', f'-p{password}', '-spf', '-y', zip_file_name]
    for found_file in collected_files:
        sh_err = sh_exec('7z', extra_args=args + [found_file], cae=cae)
        if sh_err:
            cae.po(f"***** error {sh_err} on saving environment into the archive {os.path.abspath(zip_file_name)}")
            cae.shutdown(33)
        cae.po(f" ==== saved {found_file} into the archive {zip_file_name}")

    cae.po(f"===== saved {len(collected_files)} environment files into the archive {zip_file_name}")
    if cae.debug:
        for file_name in collected_files:
            cae.po(f"   -  {file_name}")


if __name__ == '__main__':                                                                  # pragma: no cover
    main()
